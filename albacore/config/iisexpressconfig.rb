require 'ostruct'
require 'albacore/support/openstruct'

module Configuration
  module IISExpress
    include Albacore::Configuration

    def iisexpress
      @iisexpressconfig ||= OpenStruct.new.extend(OpenStructToHash).extend(IISExpress)
      yield(@iisexpressconfig) if block_given?
      @iisexpressconfig
    end
  end
end

